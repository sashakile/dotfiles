Function Prompt {
$title = "$($env:USERNAME) [$((Get-GitStatus).Branch)] $(Get-Location)"
    #$Host.UI.RawUI.WindowTitle = $title
    Write-Host "$([char]0x1B)]0;$title`a"
}

Import-Module PSReadLine
Import-Module posh-git
#Import-Module oh-my-posh
# Set-PoshPrompt -Theme stelbent.minimal

Set-PSReadLineOption -PredictionSource History
Set-PSReadLineOption -EditMode Emacs

Invoke-Expression (&starship init powershell)

Invoke-Expression (& {
    $hook = if ($PSVersionTable.PSVersion.Major -lt 6) { 'prompt' } else { 'pwd' }
    (zoxide init --hook $hook powershell) -join "`n"
})

Import-Module PSFzf
Import-Module posh-git
Import-Module git-aliases -DisableNameChecking

Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+t' -PSReadlineChordReverseHistory 'Ctrl+r'
