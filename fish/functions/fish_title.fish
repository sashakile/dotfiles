function fish_title
 set -q argv[1]; or set argv fish
 echo $USER (prompt_pwd) (fish_git_prompt): $argv;
end