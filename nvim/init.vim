call plug#begin()

" Sensible config
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'

" Code commenter
Plug 'scrooloose/nerdcommenter'
" Better file browser
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Airline
Plug 'vim-airline/vim-airline'

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Solarized
Plug 'altercation/vim-colors-solarized'

" Zen Mode
Plug 'folke/zen-mode.nvim'

" Automatically close parenthesis, etc
Plug 'Townk/vim-autoclose'

" Window chooser
Plug 't9md/vim-choosewin'

" Movement
Plug 'ggandor/lightspeed.nvim'

" Completion
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

" Initialize plugin system
call plug#end()


syntax enable
set background=dark
colorscheme solarized

" tabs and spaces handling
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" show line numbers
set nu

" Window Chooser ------------------------------

" mapping
nmap  -  <Plug>(choosewin)
" show big letters
let g:choosewin_overlay_enable = 0

lua << EOF
  require("zen-mode").setup {
    -- your configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below
  }
EOF
