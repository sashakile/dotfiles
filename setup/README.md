# Useful package manager commands

## search for software

`winget search <something>`
 
## show information of the package
`winget show --id <Package.Id>`
 
## search for software
`scoop search <something>`
 
## show information of the package
`scoop info <package-name>`
 
## open package home page in browser
`scoop home <package-name>`
